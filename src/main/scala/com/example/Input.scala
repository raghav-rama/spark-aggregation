package com.example

import scala.io.StdIn

object Input {
  def readInt(): Int = {
    print("Enter a number: ")
    val input = StdIn.readInt()
    input
  }
}
