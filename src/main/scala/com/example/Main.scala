package com.example

import org.apache.spark._
import com.example.Input.readInt
import com.example.AverageRainfall.run

object Main {
  def main(args: Array[String]): Unit = {
    run()
  }
}
