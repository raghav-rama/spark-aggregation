package com.example

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object AverageRainfall {
  def run(): Unit = {
    // SparkSession Setup
    val spark = SparkSession.builder
      .master("local[*]")
      .appName("SparkWindowedStreaming")
      .getOrCreate()

    import spark.implicits._
    spark.conf.set("spark.sql.shuffle.partitions", 5)

    // Schema Definition for Input Data
    val userSchema = new StructType()
      .add("Creation_Time", "double")
      .add("Station", "string")
      .add("Rainfall", "float")

    // Read Streaming JSON Data
    val streaming = spark.readStream
      .schema(userSchema)
      .json("threewindows")

    // Convert Creation_Time to Timestamp (event_time) and Windowing
    val withEventTime = streaming
      .selectExpr(
        "cast(cast(Creation_Time as double)/1000000000 as timestamp) as event_time",
        "*" // Include all other columns
      )
      .groupBy(
        window(col("event_time"), "15 minutes") // Windowing by 15 minutes
      )
      .agg(
        avg("Rainfall").alias("avg_rainfall"),
        count("Station").alias("num_stations")
      )

    // Write Results to Console (For Demonstration)
    val query = withEventTime.writeStream
      .outputMode("complete") // Output all aggregates per window
      .format("console")
      .option("truncate", false)
      .start()

    query.awaitTermination()
  }
}
