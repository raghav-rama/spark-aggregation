val scala2Version = "2.13.14"
val sparkVersion = "3.5.1"
val hadoopVersion = "3.3.4"

lazy val root = project
  .in(file("."))
  .settings(
    name := "test-project",
    version := "0.1.0-SNAPSHOT",
    scalaVersion := scala2Version,
    libraryDependencies += "org.scalameta" %% "munit" % "0.7.16" % Test,
    libraryDependencies += "org.apache.hadoop" % "hadoop-client" % hadoopVersion,
    libraryDependencies ++= Seq(
      "org.apache.spark" %% "spark-core" % sparkVersion,
      "org.apache.spark" %% "spark-sql" % sparkVersion,
      "org.apache.spark" %% "spark-mllib" % sparkVersion
    ),
    logLevel := Level.Warn
  )
